//Khi người dùng vào website hiển thị hộp thoại prompt yêu cầu người dùng nhập tuổi
//Nếu người dùng nhập 0-15 tuổi thì chuyển sang trang 15age.html
//Nếu người dùng nhập 16-30 tuổi thì chuyển sang trang 30age.html
//Nếu người dùng nhập > 30 tuổi thì chuyển sang trang 50age.html
//Thêm 1 btn back về trang chủ ở các trang 15age, 30age, 50age
//Yêu cầu 1: code lại từ đầu bài học buổi ngày 9/11
//Yêu cầu 2: đẩy bài tập lên git và gửi link git cho anh
//Yêu cầu 3: xem lại video function trên f8 và code lại ví dụ
var age15 = 15;
var age30 = 30;
var result = prompt("Enter your age?");
var age = Number(result);
var less15Age = age <= age15;
var than15Age = age > age15 && age < age30;

function redirectPage(url) {
    window.location.assign(url);
}

if (less15Age) {
    redirectPage("15age.html");
} else if (than15Age) {
    redirectPage("30age.html");
} else {
    redirectPage("50age.html");
}

